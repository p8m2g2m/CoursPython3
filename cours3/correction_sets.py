fruits = {'pomme', 'banane', 'mangue', 'poire', 'fraise', 'abricot', 'ananas'}

legumes = {'pomme de terre', 'carotte', 'choux', 'navet', 'patate douce', 'tomate'}

liste_test = ['navet', 'mangue', 'framboise', 'choux', 'poire', 'citron']

for vegetal in liste_test:
    if vegetal in fruits:
        print(vegetal, 'est un fruit.')
    elif vegetal in legumes:
        print(vegetal,'est un légume.')
    else:
        print(vegetal, "n'est dans aucune liste")

