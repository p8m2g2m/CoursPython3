bilbo = {
    "titre" : "Bilbo le Hobbit",
    "auteur": "J.R.R. Tolkein",
    "ISBN":   2253049417,
    "parution": 1954,
    "collection": "Imaginaire",
    "pagination": 384
}

print(bilbo)
print('*'*30)
bilbo['traduction'] = 'Francis Ledoux'
print(bilbo)
print('*'*30)
bilbo.pop('collection')
print(bilbo)
print('*'*30)
bilbo.update({'traduction': 'Francis Ledoux, janvier 1989'})
print(bilbo)
print('*'*30)
print('ISBN du livre:', bilbo['ISBN'])
print('ISBN du livre:', bilbo.get('ISBN'))

print('*'*30)
valeur = 384
for key, value in bilbo.items():
    if value == valeur:
        print('La valeur', valeur, 'se trouve à la clé', key,'.')
 
print('*'*30)

for element1, element2 in bilbo.items():
    print(element1.title(), ":", (str(element2) + ";"))

