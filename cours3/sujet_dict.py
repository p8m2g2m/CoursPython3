bilbo = {
    "titre" : "Bilbo le Hobbit",
    "auteur": "J.R.R. Tolkein",
    "ISBN":   2253049417,
    "parution": 1954,
    "collection": "Imaginaire",
    "pagination": 384
}

print(bilbo)
print('*'*30)

# ajouter l'élément Traduction: Francis Ledoux

#print(bilbo)
#print('*'*30)

# supprimer l'élément collection
#print(bilbo)
#print('*'*30)

# mettre à jour l'élément traduction: Francis Ledoux, janvier 1989

#print(bilbo)
#print('*'*30)


## Afficher l'ISBN du livre

#print('*'*30)
 
## à quelle clé correspond la valeur 384 ?

#print('*'*30)


# Afficher proprement tous les éléments du dictionnaire

