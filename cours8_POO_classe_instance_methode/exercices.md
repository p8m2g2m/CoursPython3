# Exercice  sur les classes et méthodes de classe

Un employé d'une entreprise voit son salaire calculé en fonction de la valeur du point (indexé sur le coût de la vie), le nombre de points (selon ses responsabilités -cadre ou non cadre-, sa place dans la hiérarchie, etc) 
et son ancienneté. En 2019, on vous demande de créer un programme permettant de calculer le salaire d'un employé.

- Créer une classe Employe qui contient les attributs suivants:
  * valeur_point: 55
  * taux_anciennete: 1.00
  * nom : chaine de caractères vide
  * prenom : chaine de caractères vide
  * poste : chaine de caractères vide
  * nombre_points : 0
  
- Créer le constructeur de la classe qui prend les arguments suivants:
  * nom
  * prenom
  * poste
  * nombre_points
  * majoration_ancienneté (reprise du taux d'ancienneté de l'ancien système)

- Créer la méthode getSalaireBrut() qui renvoit le salaire brut d'un employé avec la formule suivante: valeur_point nombre_point taux_anciennete
- Créer la méthode getSalaireNet() qui renvoit le salaire net d'un employé avec la formule suivante: salaire brut * 0.77
- Créer la méthode setTauxAnciennete qui permet d'ajouter une valeur comprise entre 0 et 1 au taux_anciennete de l'employé avec un maximum à 1.25
- Créer la méthode setNbrePoint qui permet de définir une nouvelle valeur entière de nombre de points.

- Créer l'instance marcel : Robert Bidochon, soudeur, 37 points, ancienneté: +.15
  * Exemple:
    robert = Employe(prenom= 'Robert', nom = 'Bidochon', poste = 'soudeur', nombre_points = 37, majoration = 0.15)
- Vérifier que le salaire net mensuel de Robert est d'environ 1802€.

- Créer l'instance raymonde: Raymonde Galopin, dactylo, points: 38, majoration +0.20
- Créer l'instance theophile: Théophile Gauthier, secrétaire, points: 28 , majoration +0.0

- Calculer le salaire de chacun des employés, les sauvegarder dans un fichier csv salaire_employes.csv avec les colonnes annee (ici 2019), nom, prenom, salaire brut annuel séparés par un point-virgule.

- En 2020, la valeur du point passera à 56.5; Théophile aura une augmentation de 3 points, tous gagnent 1% d'ancienneté.
  Ajouter les salaires prévisionnels de 2020 à la suite du fichier salaire_employes.csv

- Rendus attendus pour ceux qui veulent un retour sur leur code:
  * script (ou notebook avec des commentaires )!
  * fichier csv
